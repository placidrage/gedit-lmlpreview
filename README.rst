..


====================================
Lightweight Markup Languages preview
====================================

A gedit plugin to preview your text written in a L. M. L.
So far the supported languages are reST, markdown, textile and creole.

This plugin was primarily designed to be easily extensible to support other markup languages. However, performance is a concern, and will be looked at when
the important functionalities have been implemented.


Installing
----------

We are trying our best to simplify the process, but right now, these are the
steps to follow:

- copy the folder **lml-preview** and the file **lml-preview.plugin** to your
  gedit plugin folder, preferably in:
  ~/.local/share/gedit/plugins, since this plugin isn't fully backed yet.

- setup a virtualenv in the folder lml-preview and install the python
  requirements::

      $ cd ~/.local/share/gedit/plugins/lml-preview
      $ virtualenv --distribute .
      $ . bin/activate
      $ pip install -r requirements.txt

This setup allows you to install the requirements in a sandbox, without
littering your system folders, in case you want to uninstall, just delete the
files and folders your copied and you're good.


Testing
-------

Various files copied from various places are in the tests folder for you to test
the preview function.


TODO
----

- Add a way to add conversion tools with each markup language support handler.

- A lot of data is generated at runtime to simplify the extensibility, but
  maybe the same goal can be achieved by generating the data statically to
  avoid performance problems.

- Find a way to properly pack and install any gtksourceview styles and language
  specs with the plugin.

  - pbor on irc pointed out that with ``g_get_user_data_dirs`` might allow to
    get xdg dirs at runtime

