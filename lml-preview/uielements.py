# -*- coding: utf-8 -*-


# -----------------------------------------------------------------------------

returned_actions = False

# -----------------------------------------------------------------------------

ui_str = \
"""<ui>
    <menubar name="MenuBar">
        <menu name="ToolsMenu" action="Tools">
            <separator/>
{menu_entry}
            <separator/>
        </menu>
    </menubar>
</ui>
"""

# -----------------------------------------------------------------------------

menu_ui = \
"""                <placeholder name="ToolsOps_6">
                    <menu name="{menu_name}Menu" action="{action_name}">
                        {menu_item}
                    </menu>
                </placeholder>
{menu_entry}"""

mi_ui = \
"""<menuitem name="{mi_name}" action="{action_name}"/>"""

# -----------------------------------------------------------------------------

menu_actions = []

# -----------------------------------------------------------------------------

def add_menus(name):
    global ui_str
    global menu_actions
    global menu_ui

    tmp_name = name+"_action"

    menu_actions.append((
        tmp_name,
        None,
        name,
        None,
        None,
        None,
    ),)

    ui_str = ui_str.format(
        menu_entry=menu_ui,
        menu_item=""
    ).format(
        menu_name=name,
        action_name=tmp_name,
        menu_item="{menu_item}",
        menu_entry="{menu_entry}"
    )

# -----------------------------------------------------------------------------

def add_action(name, klass, funktor=None):
    """
    Add a UI element to the menu
    """
    global ui_str
    global menu_actions
    global mi_ui

    if funktor is None:
        funktor = klass

    menu_actions.append((
        name.format("action"),
        None,
        klass.label,
        klass.accelerator,
        klass.tooltip,
        funktor,
    ),)

    ui_str = ui_str.format(
        menu_item=mi_ui.format(
            mi_name=name.format("mi"),
            action_name=name.format("action")
        ),
        menu_entry="{menu_entry}"
    )

# -----------------------------------------------------------------------------

def get_actions():
    global returned_actions

    returned_actions = True

    return menu_actions

# -----------------------------------------------------------------------------

def get_ui():
    """
    Get the final UI
    """
    global returned_actions
    global ui_str

    if returned_actions:
        return ui_str.format(menu_item="", menu_entry="")
    else:
        print("uielements: you should retrieve and register actions before ui")

# -----------------------------------------------------------------------------
