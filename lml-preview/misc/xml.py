
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

# -----------------------------------------------------------------------------

def build_menu():
    func_list = [
        "preview",
    ]

    # -------------------------------------------------------------------------

    # Menu item example, insert a new item in the Tools menu
    ui_root_elem = ET.Element('ui')

    menubar_elem = ET.SubElement(ui_root_elem, 'menubar')
    menubar_elem.set('name', 'MenuBar')

    # -------------------------------------------------------------------------

    menu_elem = ET.SubElement(menubar_elem, 'menu')
    menu_elem.set('name', 'ToolsMenu')
    menu_elem.set('action', 'Tools')

    # -------------------------------------------------------------------------

    ET.SubElement(menu_elem, 'separator')

    # -------------------------------------------------------------------------

    # Generate menuitem placeholders
    for toolops, func_name in enumerate(func_list, 6):
        ph = ET.SubElement(menu_elem, 'placeholder')
        ph.set('name', 'ToolsOps_{0}'.format(toolops))

        mi = ET.SubElement(ph, 'menuitem')
        mi.set('name', func_name)
        mi.set('action', func_name)

    # -------------------------------------------------------------------------

    ET.SubElement(menu_elem, 'separator')

    # -------------------------------------------------------------------------

    return ET.tostring(ui_root_elem, encoding="utf-8", method="xml")

# -----------------------------------------------------------------------------

