# -*- coding: utf-8 -*-

from gettext import gettext as _

from textile import textile


# -----------------------------------------------------------------------------

# markup name
name = "textile"

# -----------------------------------------------------------------------------

# TODO: add html, s5, odt, latex, man output and
# add html import
format_pairs = (
    ("textile", "preview"),
)

# -----------------------------------------------------------------------------

class _Textile2Preview(object):

    def __init__(self):
        self.label = _("textile preview")
        self.accelerator = None
        self.tooltip = _("preview textile document")

    def __call__(self, action, user_data):
        lmlpreview = user_data[0]

        text = lmlpreview.get_selection()
        text = unicode(text, 'utf-8')

        html = textile(text)
        lmlpreview.do_render(html)

textile2preview = _Textile2Preview()

# -----------------------------------------------------------------------------
