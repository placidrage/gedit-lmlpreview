# -*- coding: utf-8 -*-

from gettext import gettext as _

from markdown import markdown


# -----------------------------------------------------------------------------

# markup name
name = "markdown"

# -----------------------------------------------------------------------------

# TODO: add html, s5, odt, latex, man output and
# add html import
format_pairs = (
    ("md", "preview"),
)

# -----------------------------------------------------------------------------

class _Md2Preview(object):

    def __init__(self):
        self.label = _("markdown preview")
        self.accelerator = None
        self.tooltip = _("preview markdown document")
        self.version = "standard"

    def __call__(self, action, user_data):
        lmlpreview = user_data[0]

        text = lmlpreview.get_selection()
        text = unicode(text, 'utf-8')

        if self.version == "standard":
            html = markdown(
                text,
                smart_emphasis=False
            )
        else:
            html = markdown(
                text,
                extensions=['extra', 'headerid(forceid=False)']
            )
        lmlpreview.do_render(html)

md2preview = _Md2Preview()

# -----------------------------------------------------------------------------
