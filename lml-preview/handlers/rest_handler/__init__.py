#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from mako.template import Template
from docutils.core import publish_parts
from docutils.writers import html4css1
from docutils.writers.html4css1 import Writer

from makeTable import toRSTtable

## pygments support
import highlighter

## setup resources paths
from utils import *

# TODO: add management for mako_modules temp folder


# -----------------------------------------------------------------------------

# markup name
name = "reST"

# -----------------------------------------------------------------------------

## I'm not satisfied with that
#css = os.path.join(resources_path,'restmain.css')
css = os.path.join(
    os.path.dirname(html4css1.__file__),
    Writer.default_stylesheet
)

styles = open(css,'r')

# -----------------------------------------------------------------------------

# TODO: add html, s5, odt, latex, man output and
# add html import
format_pairs = (
    ("reST", "preview"),
)

# -----------------------------------------------------------------------------


class _Rst2Preview(object):
    def __init__(self):
        self.label = _("reSt preview")
        self.accelerator = "<Control><Shift>R"
        self.tooltip = _("preview reSt document")

    def __call__(self, action, user_data):
        lmlpreview = user_data[0]

        text = lmlpreview.get_selection()
        text = unicode(text, 'utf-8')

        html_body = publish_parts(text, writer_name="html")["html_body"]

        template_file = os.path.join(templates_path, 'base.html')
        mytemplate = Template(
            filename=template_file,
#            module_directory=windowdata["mako_temp"]
        )

        html = mytemplate.render(
            title=self.label,
            styles=styles.read(),
            body=html_body
        )

        lmlpreview.do_render(html)

reST2preview = _Rst2Preview()

# -----------------------------------------------------------------------------

class _Rst2Latex(object):
    def __init__(self):
        pass

    def __call__(self, doc):
        filename = doc.get_uri_for_display()[:-4]
        pd = restpluginDir
        os.popen2(
            'python %s/to_tex.py "%s.rst" "%s.tex"' % (pd, filename, filename)
        )

# -----------------------------------------------------------------------------

class _Rst2Html(object):
    def __init__(self):
        pass

    def __call__(self, doc):
        filename = doc.get_uri_for_display()[:-4]
        pd = restpluginDir
        os.popen2(
            'python %s/to_html.py \
            --stylesheet=%s/restmain.css \
            --language=fr "%s.rst" "%s.html"' % (pd, pd, filename, filename)
        )

# -----------------------------------------------------------------------------

def _Rst2Odt(action):
    def __init__(self):
        pass

    def __call__(self):
        filename = doc.get_uri_for_display()[:-4]
        pd = restpluginDir
        os.popen2(
            'python %s/to_odt.py \
            --add-syntax-highlighting \
            --stylesheet=%s/default.odt \
            "%s.rst" "%s.odt"' % (pd, pd, filename, filename)
        )

# -----------------------------------------------------------------------------

#            default_stylesheet_path = utils.relative_path(
#                os.path.join(os.getcwd(), 'dummy'),
#                os.path.join(os.path.dirname(html4css1.__file__), Writer.default_stylesheet))

#            style_path = os.path.join(
#                os.getcwd(),
#                Writer.default_stylesheet_path
#            )

#            style = \
#                '<link rel="stylesheet" type="text/css" href="{0}" />'.format(
#                    style_path
#                )

# -----------------------------------------------------------------------------

def on_paste_code(doc):

    lines = gtk.clipboard_get().wait_for_text().split('\n')
    to_copy = "\n".join([line for line in lines[1:]])
    doc.insert_at_cursor('..sourcecode:: ChoosenLanguage\n\n    %s\n'%lines[0])
    doc.insert_at_cursor(to_copy + '\n\n')

# -----------------------------------------------------------------------------

def on_create_table(view):
    if not view:
        return

    indent = view.get_indent()

    doc = view.get_buffer()
    #print 'language=',doc.get_language()

    start = doc.get_start_iter()
    end = doc.get_end_iter()

    if doc.get_selection_bounds():
        start = doc.get_iter_at_mark(doc.get_insert())
        end = doc.get_iter_at_mark(doc.get_selection_bound())

    text = doc.get_text(start, end, False)
    doc.delete(start, end)

    lines = text.split("\n")
    labels = lines[0].split(',')
    rows = [row.strip().split(',')  for row in lines[1:]]

    doc.insert_at_cursor(toRSTtable([labels]+rows))

# -----------------------------------------------------------------------------


# GtkActionEntry structure:

#- name
#	The name of the action.

#- stock_id
#	The stock id for the action, or the name of an icon from the icon theme.

#- label
#	The label for the action. This field should typically be marked for
#	translation, see gtk_action_group_set_translation_domain(). If label is
#	NULL, the label of the stock item with id stock_id is used.

#- accelerator
#	The accelerator for the action, in the format understood by
#	gtk_accelerator_parse().

#- tooltip
#	The tooltip for the action. This field should typically be marked for
#	translation, see gtk_action_group_set_translation_domain().

#- callback
#	The function to call when the action is activated.

#----

#    "table" = (
#        None,
#        _("Create Table"),
#        None,
#        _("Create a reSt table"),
#        self.on_create_table
#    ),
#    "sourcecode" = (
#        None,
#        _("Paste Code"),
#        None,
#        _("Paste sourcecode"),
#        self.on_paste_code
#    ),
#    "--> HTML" (
#        None,
#        _("--> HTML"),
#        None,
#        _("transform to HTML"),
#        self.on_html
#    ),
#    "--> LaTeX" = (
#        None,
#        _("--> LaTeX"),
#        None,
#        _("transform to LaTeX"),
#        self.on_latex
#    ),
#    "--> OpenOffice" = (
#        None,
#        _("--> OpenOffice"),
#        None,
#        _("transform to OpenOffice"),
#        self.on_openoffice
#    ),

# -----------------------------------------------------------------------------
