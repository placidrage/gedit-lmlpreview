#!/usr/bin/env python
# -*- coding: utf-8 -*-


from docutils import nodes
from docutils.parsers.rst import directives
from docutils.core import publish_cmdline, publish_file, Publisher, default_description

## Pygments
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter


PYGMENTS_FORMATTER = HtmlFormatter(option='nowrap')


# -----------------------------------------------------------------------------

class pygments_directive(object):
    def __init__(self, arguments = (1, 0, 1), content = 1):
        self.arguments = arguments
        self.content = content

    def __call__(self, name, arguments, options, content, lineno,
                content_offset, block_text, state, state_machine):
        try:
            lexer = get_lexer_by_name(arguments[0])
        except ValueError:
            # no lexer found
            lexer = get_lexer_by_name('text')
        parsed = highlight(u'\n'.join(content), lexer, PYGMENTS_FORMATTER)
        return [nodes.raw('', parsed, format='html')]

my_directive = pygments_directive()
directives.register_directive('sourcecode', my_directive)

# -----------------------------------------------------------------------------

import locale
try:
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

# -----------------------------------------------------------------------------

if __name__ == "__main__":
    import docutils.core
    publish_string(writer_name='html')

# -----------------------------------------------------------------------------
