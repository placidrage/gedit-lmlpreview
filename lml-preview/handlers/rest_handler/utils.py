#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os


# -----------------------------------------------------------------------------

# construct important paths
handler_path = os.path.dirname(__file__)

resources_path = os.path.join(handler_path, 'resources')
templates_path = os.path.join(resources_path, 'templates')

# -----------------------------------------------------------------------------
