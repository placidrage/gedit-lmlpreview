# -*- coding: utf-8 -*-

from gettext import gettext as _

from creole import creole2html


# -----------------------------------------------------------------------------

# markup name
name = "creole"

# -----------------------------------------------------------------------------

# TODO: add html, s5, odt, latex, man output and
# add html import
format_pairs = (
    ("creole", "preview"),
)

# -----------------------------------------------------------------------------

class _Creole2Preview(object):

    def __init__(self):
        self.label = _("creole preview")
        self.accelerator = None
        self.tooltip = _("preview creole document")

    def __call__(self, action, user_data):
        lmlpreview = user_data[0]

        text = lmlpreview.get_selection()
        text = unicode(text, 'utf-8')

        html = creole2html(text)
        lmlpreview.do_render(html)

creole2preview = _Creole2Preview()

# -----------------------------------------------------------------------------
