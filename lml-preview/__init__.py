#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import tempfile
import importlib
from shutil import rmtree
from pprint import pprint
from gettext import gettext as _

from gi.repository import GObject, Gtk, WebKit, Gedit

## activate virtualenv for mako docutils and pygments
from utils import *

## get the UI elements required
import uielements


# -----------------------------------------------------------------------------

test_html = \
"""
<!DOCTYPE html>
<html>
    <head>
        <title>lml-preview</title>
    </head>
    <body>
    </body>
</html>
"""

# -----------------------------------------------------------------------------

class lmlPlugin(GObject.Object, Gedit.WindowActivatable):
    """
    Editing reStructuredText more conveniently in gedit.
    """

    __gtype_name__ = "lmlPlugin"

    window = GObject.property(type=Gedit.Window)

    # -------------------------------------------------------------------------

    def __init__(self):
        GObject.Object.__init__(self)

        print("lmlPlugin: initializing plugin ...")

    # -------------------------------------------------------------------------

    def _register_handlers(self, windowdata):
        """
        Register handlers: Looks for packages in the handlers folder, imports
        them and runs the registration function of each.

        At the end of this method, you should have a two attributes for use
        :lml_handlers: a dictionary where you can lookup imported package by
                       name
        :format_manager: a dictionary where you can lookup supported format
                         output by supported format input.
        """
        # List of folders in the handlers folder to be treated as packages to
        # import
        lml_handlers_names = [
            name for name in os.listdir(lml_handlers_path)
            if os.path.isdir(os.path.join(lml_handlers_path, name))
        ]

        # Create attributes
        self.lml_handlers = dict()
        self.format_manager = dict()

        string_mask = "{0}2{1}{2}"

        # Import and register handlers
        for handler in lml_handlers_names:
            mod = self.lml_handlers[handler] = importlib.import_module(
                ".{0}".format(handler),
                package="lml-preview.handlers"
            )

            # populate the format_manager dictionary
            tmp_format_pairs = mod.format_pairs
            if tmp_format_pairs:
                uielements.add_menus(mod.name)

                for format_input, format_output in tmp_format_pairs:
                    callback = getattr(
                        mod,
                        string_mask.format(format_input, format_output, "")
                    )
                    if callback:
                        self.format_manager[format_input] = \
                            dict(((format_output, callback),))

                        # Add action to action list and ui element to the menu ui
                        uielements.add_action(
                            string_mask.format(
                                format_input,
                                format_output,
                                "_{0}"
                            ),
                            callback
                        )

                        callback = None

        # Add action group to windowdata
        windowdata["action_group"] = Gtk.ActionGroup("lmlPluginActions")
        windowdata["action_group"].add_actions(
            uielements.get_actions(),
            (self, )
        )

        # Get UI manager
        manager = self.window.get_ui_manager()

        # Insert the action group
        manager.insert_action_group(windowdata["action_group"], -1)

        # Merge the UI
        windowdata["ui_id"] = manager.add_ui_from_string(uielements.get_ui())

    # -------------------------------------------------------------------------

    def get_selection(self):
        """
        Handles text selections from the document passed as an argument.

        :doc: The document from which to perform the selection
        :returns: the selected text, or the whole text otherwise
        """
        doc = self.window.get_active_document()
        bounds = doc.get_selection_bounds()
        if bounds:
            start, end = bounds
        else:
            start = doc.get_start_iter()
            end = doc.get_end_iter()

        text = doc.get_text(start, end, False)

        return text

    # -------------------------------------------------------------------------

    def do_activate(self):
        print("lmlPlugin: activating plugin ...")

        # Initialize plugin's temp folder
        lml_temp = tempfile.mkdtemp(prefix="lml_preview")

        # Initialize and setup a scrolled window
        scrolled_window = Gtk.ScrolledWindow()

        scrolled_window.set_property(
            "hscrollbar-policy",
            Gtk.PolicyType.AUTOMATIC
        )
        scrolled_window.set_property(
            "vscrollbar-policy",
            Gtk.PolicyType.AUTOMATIC
        )

        scrolled_window.set_property("shadow-type", Gtk.ShadowType.IN)

        # Initialize and setup a webkit view
        html_view = WebKit.WebView()
        html_view.props.settings.props.enable_default_context_menu = False
        html_view.load_string(
            (test_html),
            "text/html", "utf-8", "file:///"
        )

        scrolled_window.add(html_view)
        scrolled_window.show_all()

        panel = self.window.get_bottom_panel()

        image = Gtk.Image()
        image.set_from_icon_name("gnome-mime-text-html", Gtk.IconSize.MENU)
        panel.add_item(
            scrolled_window,
            "lml Preview",
            _("LML Preview"),
            image
        )

        # Store data in the window object
        windowdata = dict()
        self.window.set_data("lmlPreviewData", windowdata)

        # Register handlers
        self._register_handlers(windowdata)

        windowdata["lml_temp"] = lml_temp
        windowdata["scrolled_window"] = scrolled_window
        windowdata["html_view"] = html_view

#        doc = self.window.get_active_document()
#        lang = doc.get_language()
#        print("\nLanguage name")
#        print(lang.get_name())

#        print("\nLanguage ID")
#        print(lang.get_id())

#        print("\nLanguage mime type")
#        print(lang.get_mime_types())

#        print("\nLanguage Style IDs")
#        print(lang.get_style_ids())

#        print("\nLanguage Section:")
#        print(lang.get_section())

    # -------------------------------------------------------------------------

    def do_deactivate(self):
        # Retreive the data of the window object
        windowdata = self.window.get_data("lmlPreviewData")

        # Remove the menu action
        manager = self.window.get_ui_manager()
        manager.remove_ui(windowdata["ui_id"])
        manager.remove_action_group(windowdata["action_group"])

        # Remove the bottom panel
        panel = self.window.get_bottom_panel()
        panel.remove_item(windowdata["scrolled_window"])

        rmtree(windowdata["lml_temp"])

    # -------------------------------------------------------------------------

    def do_update_state(self):
        """
        Update sensitivity of plugin's actions.
        """
        # Retreive the data of the window object
        windowdata = self.window.get_data("lmlPreviewData")

        doc = self.window.get_active_document()

        windowdata["action_group"].set_sensitive(doc is not None)

    # -------------------------------------------------------------------------

    def do_highlight_updated(self, document, start, end):
        print("\n\nHighlight is updated\n\n")

    # -------------------------------------------------------------------------

    # Menu activate handlers
    def do_render(self, html):
#        self.do_refresh_plugins()
        # Retreive the window object data
        windowdata = self.window.get_data("lmlPreviewData")

        p = windowdata["scrolled_window"].get_placement()

        windowdata["html_view"].load_string(
            html,
            "text/html", "utf-8", "file:///"
        )

        windowdata["scrolled_window"].set_placement(p)

        self.window.get_bottom_panel().show()

# -----------------------------------------------------------------------------
