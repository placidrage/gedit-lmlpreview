#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os


# -----------------------------------------------------------------------------

# construct important paths
plugin_path = os.path.dirname(__file__)
lml_handlers_path = os.path.join(plugin_path, 'handlers')
bin_path = os.path.join(plugin_path, 'bin')

#resources_path = os.path.join(plugin_path, 'resources')
#templates_path = os.path.join(resources_path, 'templates')

# -----------------------------------------------------------------------------

# virtualenv activation hack
_activate_this = os.path.join(bin_path, 'activate_this.py')
execfile(_activate_this, dict(__file__=_activate_this))

# -----------------------------------------------------------------------------
